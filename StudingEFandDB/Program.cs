﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using LibraryForDB;
using System.Data.Entity.Validation;

namespace StudingEF
{
    public class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new FiguresContext())
            {
                ctx.Circles.Add(new Circle() { Name = "Little Circle", Radius = 5 });

                ctx.SaveChanges();
            }



        }
    }
}
