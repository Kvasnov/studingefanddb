﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryForDB
{
    public class Triangle : EntityBase
    {
        //public int TriangleId { get; set; }
        public string Name { get; set; }
        public double Cathetus1 { get; set; }
        public double Cathetus2 { get; set; }
        public double Hypotenuse { get; set; }

        public double FigureArea()
        {
            return Cathetus1 + Cathetus2 + Hypotenuse;
        }

        public double Perimeter()
        {
            return 1.0 / 2.0 * Cathetus1 * Cathetus2;
        }
    }
}
