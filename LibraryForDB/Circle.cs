﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryForDB
{
    public class Circle: EntityBase
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CircleId { get; set; }
        public string Name { get; set; }
        public double Radius { get; set; }

        public double FigureArea()
        {
            return 2 * Radius * Math.PI;
        }

        public double Perimeter()
        {
            return Math.PI * Radius * Radius;
        }
    }
}
