﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace LibraryForDB
{
    public class FiguresContext : DbContext
    {
        public FiguresContext() : base("FigureDB")
        {

        }

        public DbSet<Circle> Circles { get; set; }
        public DbSet<Triangle> Triangles { get; set; }
        public DbSet<Rectangle> Rectangles { get; set; }
    }
}
